package com.guemi.app.controller;

import com.guemi.app.model.AppUser;
import com.guemi.app.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// default login page with a post
// localhost:8080/login

@RestController
public class AppUserController {

    private AppUserService appUserService;

    @Autowired
    public AppUserController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    // Method to get all user
    @GetMapping("auth/v1/user/update/users")
    public ResponseEntity<Object> getUsers(){
        List<AppUser> appUsers = this.appUserService.getAllUser();
        return new ResponseEntity<Object>(appUsers,HttpStatus.OK) ;
    }

    // Method to create new user
    @PostMapping("auth/v1/user/update/add")
    public AppUser addNewUser(@RequestBody AppUser appUser){
        if(appUser!=null){
            return this.appUserService.addAppUser(appUser);
        }
        return null;
    }

    // Method to delete a user
    @DeleteMapping("auth/v1/user/update/delete")
    public AppUser deleteUser(@RequestBody AppUser appUser){
        if(appUser!=null){
            return this.appUserService.deleteUser(appUser);
        }
        return null;
    }

    @PutMapping("auth/v1/user/update/update")
    public AppUser updateUser(@RequestBody AppUser appUser){

        return null;
    }

}
