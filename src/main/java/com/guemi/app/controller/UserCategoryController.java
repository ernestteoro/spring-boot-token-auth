package com.guemi.app.controller;

import com.guemi.app.model.UserCategory;
import com.guemi.app.service.UserCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserCategoryController {
    private UserCategoryService userCategoryService;

    @Autowired
    public UserCategoryController(UserCategoryService userCategoryService) {
        this.userCategoryService = userCategoryService;
    }

    // endPoint to create a new user category
    @PostMapping("auth/v1/update/addUserCategory")
    public UserCategory addNewUserCategory(@RequestBody UserCategory userCategory){
        if (userCategory!=null){
            return this.userCategoryService.addNewUserCategory(userCategory);
        }
        return null;
    }

    // endPoint to get user caterigories
    @GetMapping("auth/v1/list/userCategories")
    @ResponseBody
    public ResponseEntity<?> getUserCategories(){

        List<UserCategory> userCategoryList =this.userCategoryService.getAllUserCategory();
        return new ResponseEntity<Object>(userCategoryList, HttpStatus.OK);
    }

    // endPoint to delete a user category
    @DeleteMapping("auth/v1/update/deleteUserCategory")
    @ResponseBody
    public UserCategory deleteUserCategory(@RequestBody UserCategory userCategory){

        if (userCategory!=null){
            return this.userCategoryService.deleteUserCategory(userCategory);
        }

        return null;
    }

}
