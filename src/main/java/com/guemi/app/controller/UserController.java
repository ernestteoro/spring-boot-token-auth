package com.guemi.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.plugin.liveconnect.SecurityContextHelper;

/**
 *
 * @author teoro
 */

@RestController
public class UserController {

    @GetMapping("auth/v1/list/testCustomer")
    public ResponseEntity<?> testUserController(){
        return new ResponseEntity<>("This is a test for user controller!",HttpStatus.OK);
    }

    @GetMapping("auth/v1/list/helloword")
    public ResponseEntity<?> hello(){
        return new ResponseEntity<>("Hello World!",HttpStatus.OK);
    }
}