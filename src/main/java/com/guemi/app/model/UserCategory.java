package com.guemi.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the user_category database table.
 * 
 */
@Entity
@Table(name="user_category")
public class UserCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_category_id", unique=true, nullable=false)
	private int userCategoryId;

	@Column(name="created_at", nullable=false)
	private Timestamp createdAt;

	@Column(name="created_by", length=45)
	private String createdBy;

	@Column(name="modified_at", nullable=false)
	private Timestamp modifiedAt;

	@Column(name="modified_by", length=45)
	private String modifiedBy;

	@Column(name="user_category_description", length=255)
	private String userCategoryDescription;

	@Column(name="user_category_name", nullable=false, length=255)
	private String userCategoryName;

	//bi-directional many-to-one association to AppUser
	@OneToMany(mappedBy="userCategory")
	@JsonIgnore
	private List<AppUser> appUsers;

	public UserCategory() {
	}

	public int getUserCategoryId() {
		return this.userCategoryId;
	}

	public void setUserCategoryId(int userCategoryId) {
		this.userCategoryId = userCategoryId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getUserCategoryDescription() {
		return this.userCategoryDescription;
	}

	public void setUserCategoryDescription(String userCategoryDescription) {
		this.userCategoryDescription = userCategoryDescription;
	}

	public String getUserCategoryName() {
		return this.userCategoryName;
	}

	public void setUserCategoryName(String userCategoryName) {
		this.userCategoryName = userCategoryName;
	}

	public List<AppUser> getAppUsers() {
		return this.appUsers;
	}

	public void setAppUsers(List<AppUser> appUsers) {
		this.appUsers = appUsers;
	}

	public AppUser addAppUser(AppUser appUser) {
		getAppUsers().add(appUser);
		appUser.setUserCategory(this);

		return appUser;
	}

	public AppUser removeAppUser(AppUser appUser) {
		getAppUsers().remove(appUser);
		appUser.setUserCategory(null);

		return appUser;
	}

}