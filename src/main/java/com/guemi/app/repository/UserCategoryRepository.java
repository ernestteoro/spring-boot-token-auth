/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guemi.app.repository;

import com.guemi.app.model.UserCategory;
import org.springframework.data.repository.CrudRepository;


/**
 *
 * @author teoro110501
 */
public interface UserCategoryRepository extends CrudRepository<UserCategory,Long>{
    
}
