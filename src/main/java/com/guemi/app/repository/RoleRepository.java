package com.guemi.app.repository;

import com.guemi.app.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role,Long> {
    
}
