package com.guemi.app.repository;

import com.guemi.app.model.UserDetail;
import org.springframework.data.repository.CrudRepository;

public interface UserDetailRepository extends CrudRepository<UserDetail,Long> {
}
