package com.guemi.app.pkg;

import java.util.Date;

public class HttpResponse {
    private String path;
    private String status;
    private int error;
    private String message;
    private Date timeStamp;

    // Constructors
    public HttpResponse(String path, String status, int error, String message, Date timeStamp) {
        this.path = path;
        this.status = status;
        this.error = error;
        this.message = message;
        this.timeStamp = timeStamp;
    }

    // Getters and Setters
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
