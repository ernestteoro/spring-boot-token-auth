package com.guemi.app.pkg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class Util {

    public String save(String fileName, MultipartFile file) {
        String[] nomFile;
        nomFile = file.getName().split("\\\\");
        String nf = "_" + fileName + "_" + nomFile[nomFile.length - 1];
        String path = "", retour = "KO";
        path = "/home/fics/";

        try {
            File targetFolder = new File(path + nf);
            OutputStream out = new FileOutputStream(targetFolder);
            int read = 0;
            byte[] bytes = new byte[1024];
            InputStream ips = (InputStream) file.getInputStream();
            InputStreamReader ipsr = new InputStreamReader(ips);
            while ((read = ips.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
            retour = "OK";
        } catch (IOException ex) {
            System.err.println("erreur sur IOException =" + ex.getMessage());
            retour = "NOK";
        }

        return nf;
    }
}
