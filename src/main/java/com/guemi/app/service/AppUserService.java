package com.guemi.app.service;

import com.guemi.app.model.AppUser;
import com.guemi.app.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppUserService implements UserDetailsService {
    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    private AppUserRepository appUserRepository;
    private RoleService roleService;
    private List<String> userRoles=new ArrayList<>();

    @Autowired
    public AppUserService(AppUserRepository appUserRepository, RoleService roleService) {

        this.appUserRepository = appUserRepository;
        this.roleService = roleService;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        AppUser user = loadUserByUserLogin(login);
        User user1=null;
        if (user!=null){
            userRoles = this.roleService.getUserRoles(user);
            String[] roles = new String[userRoles.size()];
            if(userRoles!=null){

                user1= new User(user.getLogin(),encoder.encode(user.getPassword()),
                        AuthorityUtils.createAuthorityList(userRoles.toArray(roles)));
            }
        }
        if(user1!=null)
            return user1;

        return null;
    }

    public AppUser loadUserByUserLogin(String userLogin){
        AppUser appUser=null;
        if (userLogin!=null){
            appUser = this.appUserRepository.findAppUsersByLogin(userLogin);

        }
        return appUser;
    }

    // Method to add new User
    public AppUser addAppUser(AppUser appUser){

        if(appUser!=null){
            return this.appUserRepository.save(appUser);
        }
        return null;
    }

    // Method to get all app user
    public List<AppUser> getAllUser(){
        List<AppUser> appUsers = new ArrayList<>();
        appUsers = (List<AppUser>) this.appUserRepository.findAll();
        return appUsers;

    }

    // Method to get all app user
    public AppUser deleteUser(AppUser appUser){
        if (appUser!=null){
            this.appUserRepository.delete(appUser);
            return appUser;
        }
        return null;
    }
}
