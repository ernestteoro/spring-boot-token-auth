package com.guemi.app.service;

import com.guemi.app.model.AppUser;
import com.guemi.app.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    // Method to get user roles
    public List<String> getUserRoles(AppUser user){

        List<String> roleList = new ArrayList<>();
        String query = "select * from role r,user_role ur,app_user au where " +
                "r.role_id = ur.role_id and ur.app_user_id = au.id_app_user and " +
                "au.id_app_user ="+user.getIdAppUser();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.query(query, new ResultSetExtractor() {

            @Override
            public List extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()){
                    roleList.add(rs.getString("role_name"));

                }
                return roleList;
            }
        });

        return roleList;
    }
}

