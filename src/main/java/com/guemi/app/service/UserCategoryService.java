/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guemi.app.service;

import com.guemi.app.model.UserCategory;
import com.guemi.app.repository.UserCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author teoro
 */

@Component
public class UserCategoryService {
    private UserCategoryRepository userCategoryRepository;

    @Autowired
    public UserCategoryService(UserCategoryRepository userCategoryRepository) {
        this.userCategoryRepository = userCategoryRepository;
    }

    // Method to add new User category to db
    public UserCategory addNewUserCategory(UserCategory userCategory){
        if (userCategory!=null){
            return this.userCategoryRepository.save(userCategory);
        }
        return  null;
    }

    // Method to get all user category
    public List<UserCategory> getAllUserCategory(){
        List<UserCategory> userCategoryList = (List<UserCategory>) this.userCategoryRepository.findAll();
        if (userCategoryList!=null && userCategoryList.size()>0){
            return userCategoryList;
        }
        return null;
    }

    // Method to delete user category
    public UserCategory deleteUserCategory(UserCategory userCategory){
        if (userCategory!=null){
            this.userCategoryRepository.delete(userCategory);
            return userCategory;
        }
        return  null;
    }
}
