package com.guemi.app.config;

import com.guemi.app.model.AppUser;
import com.guemi.app.service.AppUserService;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.guemi.app.config.SecurityConstants.HEADER_STRING;
import static com.guemi.app.config.SecurityConstants.SECRET;
import static com.guemi.app.config.SecurityConstants.TOKEN_PREFIX;

public class SecurityAuthorizationFilter extends BasicAuthenticationFilter {
    //private final CustomUserDetailsService userDetailsService;
    private final AppUserService appUserService;

    /*
    public SecurityAuthorizationFilter(AuthenticationManager authenticationManager,
                                       CustomUserDetailsService userDetailsService) {
        super(authenticationManager);
        this.userDetailsService = userDetailsService;
    }

    */
    @Autowired
    public SecurityAuthorizationFilter(AuthenticationManager authenticationManager,
                                       AppUserService appUserService) {
        super(authenticationManager);
        this.appUserService = appUserService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_STRING);
        if(header==null || !header.startsWith(TOKEN_PREFIX)){
            chain.doFilter(request,response);
            return;
        }

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = getTokenOnUserAuthentication(request);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        chain.doFilter(request,response);

    }

    // Method to get token on authentication
    private UsernamePasswordAuthenticationToken getTokenOnUserAuthentication(HttpServletRequest request){
        String token = request.getHeader(HEADER_STRING);
        if(token==null){
            return null;
        }

        String userName = Jwts.parser().setSigningKey(SECRET)
                .parseClaimsJws(token.replace(TOKEN_PREFIX,""))
                .getBody().getSubject();

        UserDetails userDetails = appUserService.loadUserByUsername(userName);
        AppUser appUser = appUserService.loadUserByUserLogin(userName);
        return appUser!= null ? new UsernamePasswordAuthenticationToken(appUser.getLogin(),appUser.getPassword()): null;

    }


}
