package com.guemi.app.config;


import com.guemi.app.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@EnableWebSecurity
@Configuration
@EnableAspectJAutoProxy
@EnableGlobalMethodSecurity(securedEnabled=true,prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //private final CustomUserDetailsService customUserDetailsService;
    private final AppUserService appUserService;

    /*
    public SecurityConfig(CustomUserDetailsService customUserDetailsService,AppUserService appUserService) {
        this.customUserDetailsService=customUserDetailsService;
        this.appUserService = appUserService;
    }
     */
    @Autowired
    public SecurityConfig(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers("/**/signup/*","/**/list/*","/**/list/**")
                .permitAll()
                .antMatchers("**/update/*").hasRole("USER")
                .antMatchers("**/update/*").hasRole("ADMIN")
                .antMatchers("**/update/*").hasRole("USER")
                .antMatchers("**/update/*").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .addFilter(new SecurityAuthenticationFilter(authenticationManager()))
                .addFilter(new SecurityAuthorizationFilter(authenticationManager(),appUserService));
        //.httpBasic();
    }


}
